class ChangeUserNil < ActiveRecord::Migration[5.1]
  def up
    change_column :users, :nickname, :string, null:false
    change_column :users, :name, :string, null:false
    change_column :users, :email, :string, null:false
    change_column :posts, :content, :text, null:false
    change_column :posts, :user_id, :string, null:false
  end

  def down
    change_column :users, :nickname, :string, null:true
    change_column :users, :name, :string, null:true
    change_column :users, :email, :string, null:true
    change_column :posts, :content, :text, null:true
    change_column :posts, :user_id, :string, null:true
  end
  
end
