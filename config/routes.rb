Rails.application.routes.draw do
  get "home/top" => "home#top"
  post "login" => "sessions#create"
  post "logout" => "sessions#destroy"

  resources :posts do
    resources :likes, only: [:create, :destroy]
    resources :comments, only: [:create, :destroy]
  end
  
  resources :users
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
  