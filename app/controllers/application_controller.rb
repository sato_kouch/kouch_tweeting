class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_current_user

  private
  def set_current_user
    @current_user = User.find_by(id: session[:user_id])
  end

  def authenticate_user
    if @current_user.blank?
      redirect_to new_login_path, notice: "ログインしてください"
    end  
  end  

  def forbid_login_user
    if @current_user.present?
      redirect_to posts_path, notice: "既にログインしています"
    end
  end    

end
