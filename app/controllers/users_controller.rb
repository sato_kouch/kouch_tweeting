class UsersController < ApplicationController
  before_action :authenticate_user, only: [:index, :show, :edit, :update]
  before_action :forbid_login_user, only: [:new, :create]
  before_action :ensure_corret_user, only: [:edit, :update]
  
  def index
    @users = User.all
  end

  def show
    @user = User.find_by(id: params[:id])
    @posts = @user.posts.includes(:likes).all.order(created_at: :desc)
    @post = Post.new
  end

  def create
    @user = User.new(user_params)  
    if @user.save
      session[:user_id] = @user.id
      redirect_to user_path(@user), notice: "アカウントを作成しました"
    else
      render :new  
    end
  end

  def edit
    @user = User.find_by(id: params[:id]) 
  end

  def update
    @user = User.find_by(id: params[:id])
    if @user.update(user_params)
      redirect_to user_path(@user), notice: "ユーザー情報を更新しました"
    else
      render :edit
    end
  end    
    
  private
  def ensure_corret_user
    if session[:user_id] != params[:id].to_i
      redirect_to posts_path, notice: "他のユーザーの情報は編集できません"
    end
  end

  def user_params
    params.require(:user).permit(:nickname, :email, :password, :image)
  end  

end
