class PostsController < ApplicationController
  before_action :authenticate_user 
  before_action :ensure_corret_user_of_post, only: [:edit, :destroy]
  
  def index
    @posts = Post.includes(:user, :likes).all.order(created_at: :desc)
    @post = Post.new
  end

  def show
    @post = Post.includes(:likes).find_by(id: params[:id])
    @comments = @post.comments.includes(:user).all.order(created_at: :desc)
    @comment = Comment.new
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.user_id = @current_user.id
    if @post.save
      redirect_to posts_path, notice: "投稿が作成されました！"
    else
      render :new
    end
  end
  
  def edit
    @post = Post.find_by(id: params[:id])
  end
  
  def update
    @post = Post.find_by(id: params[:id])
    if @post.update(post_params)
     redirect_to posts_path, notice: "投稿を編集しました"
    else 
     render :edit
    end
  end

  def destroy
    @post = Post.find_by(id: params[:id])
    if @post.destroy
      redirect_to posts_path, notice: "投稿を削除しました"
    else
      redirect_to posts_path, alert: "削除に失敗しました"
    end 
  end

  private
  def post_params
    params.require(:post).permit(:content, :picture)
  end

  def ensure_corret_user_of_post
    @post = Post.find_by(id: params[:id]) 
    if @current_user != @post.user
      redirect_to posts_path, notice: "他のユーザーの投稿は編集・削除できません"
    end
  end

end