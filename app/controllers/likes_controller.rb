class LikesController < ApplicationController
  before_action :authenticate_user
  before_action :set_post

  def create
    @like = Like.new(post_id: params[:post_id], user_id: @current_user.id)
    @like.save
  end

  def destroy
    like = Like.find_by(post_id: params[:post_id], user_id: @current_user.id) 
    like.destroy
  end

  private
  def like_params
    params.permit(:post_id, :user_id)
  end

  def set_post
    @post = Post.find_by(id: params[:post_id])
  end  
end