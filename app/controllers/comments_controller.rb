class CommentsController < ApplicationController

  def create
    @comment = Comment.new(comment_params)
    @comment.user_id = @current_user.id
    @comment.post_id = params[:post_id]
    @comment.save
    @post = Post.find(params[:post_id])
    redirect_to post_path(@post), notice: "投稿が作成されました！"
  end

  private
  def comment_params
    params.require(:comment).permit(:content)
  end

end
