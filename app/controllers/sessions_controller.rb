class SessionsController < ApplicationController
  before_action :forbid_login_user, only: [:new]
  
  def create
    @user = User.find_by(email: params[:email])
    if @user && @user.authenticate(params[:password])
      session[:user_id] = @user.id
      redirect_to posts_path, notice: "ログインしました"
    else
      @email = params[:email]
      flash[:alert] = "メールアドレスかパスワードが違います"
      render "/home/top"
    end  
  end 
 
  def destroy
    reset_session
    redirect_to "/home/top", notice: "ログアウトしました"
  end  

end



