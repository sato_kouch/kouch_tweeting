$(document).ready(function(){
    $('.top-logo').css('display', 'none').fadeIn(1500);
    $('.top-content').css('display', 'none').fadeIn(5000);

    $('#signup-modal-show').click(function(){
        $('#signup-modal').fadeIn();
    });

    $('#login-modal-show').click(function(){
        $('#login-modal').fadeIn();
    });

    $('.close-modal').click(function(){
        $('#signup-modal').fadeOut();
        $('#login-modal').fadeOut();
    });
});
