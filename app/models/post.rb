class Post < ApplicationRecord
  belongs_to :user
  has_many :likes, dependent: :delete_all

  has_many :comments, dependent: :delete_all
  #default_scope -> { order(created_at: :desc) }

  mount_uploader :picture, PictureUploader
  validates :picture, { presence: true }
  validates :content, { presence: true, length: { maximum: 250 } }
  validates :user_id, { presence: true }
end


