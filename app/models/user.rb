class User < ApplicationRecord
  has_secure_password
  has_many :posts, dependent: :delete_all
  has_many :likes, dependent: :delete_all
  has_many :comments, dependent: :delete_all

  mount_uploader :image, ImageUploader
  validates :nickname, { presence: true, uniqueness: true }
  validates :email, { presence: true, uniqueness: true }
  validates :password, { presence: true } 
end
